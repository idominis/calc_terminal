package com.company;

import org.jetbrains.annotations.Contract;
import java.util.Scanner;
import java.io.*;
import java.io.BufferedReader;
import java.io.IOException;
import static java.lang.System.*;

public class Main {

    public static void main(String[] args) throws IOException {
        float broj_A, broj_B;
        char operator_c;
        int i=0;

        System.out.println("*** Jednostavni konzolni kalkulator ***");
        System.out.println("***        za operacije + - * /     ***");

	    BufferedReader brA = new BufferedReader(new InputStreamReader(in));
        System.out.println("\nMolim unesite prvi broj (A): ");
        broj_A = Float.parseFloat(brA.readLine());
        System.out.println("Unijeli ste: " + broj_A);


        System.out.println("\nMolim unesite operator: ");
        Scanner operator = new Scanner(System.in);
        operator_c = operator.next().charAt(0);

        while (i<1) {
            if (operator_c == '+') {
                System.out.println("Unijeli ste operator: " + operator_c);
                i = 2;
            }
            else if (operator_c == '-') {
                System.out.println("Unijeli ste operator: " + operator_c);
                i = 2;
            }
            else if (operator_c == '*') {
                System.out.println("Unijeli ste operator: " + operator_c);
                i = 2;
            }
            else if (operator_c == '/') {
                System.out.println("Unijeli ste operator: " + operator_c);
                i = 2;
            }
            else {
                System.out.println("Unijeli ste netočan operator!");
                System.out.println("Dozvoljeni operatori su: + - / *");
                System.out.println("\nMolim unesite operator: ");
                operator_c = operator.next().charAt(0);
                i= 0;
            }
        }

        BufferedReader brB = new BufferedReader(new InputStreamReader(in));
        System.out.println("\nMolim unesite drugi broj (B): ");
        broj_B = Float.parseFloat(brA.readLine());
        System.out.println("Unijeli ste: " + broj_B);

        switch (operator_c){
            case '+':
                System.out.println("\nRezultat: " + "A " + operator_c + " B " + " = "+ Zbroji(broj_A, broj_B));
                break;
            case '-':
                System.out.println("\nRezultat: " + "A " + operator_c + " B " + " = "+ Oduzmi(broj_A, broj_B));
                break;
            case '*':
                System.out.println("\nRezultat: " + "A " + operator_c + " B " + " = "+ Mnozi(broj_A, broj_B));
                break;
            case '/':
                System.out.println("\nRezultat: " + "A " + operator_c + " B " + " = "+ Dijeli(broj_A, broj_B));
                break;
            default:
                System.out.println("\nNepoznata radnja!");
                break;
        }
    }

    @Contract(pure = true)
    private static float Zbroji(float broj_a, float broj_b) {
        return (broj_a + broj_b);
    }

    @Contract(pure = true)
    private static float Oduzmi(float broj_a, float broj_b) {
        return (broj_a - broj_b);
    }

    @Contract(pure = true)
    private static float Mnozi(float broj_a, float broj_b) {
        return (broj_a * broj_b);
    }

    @Contract(pure = true)
    private static float Dijeli(float broj_a, float broj_b) {
        return (broj_a / broj_b);
    }
}
